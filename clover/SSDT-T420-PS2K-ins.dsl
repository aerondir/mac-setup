// Thinkpad model with TrackPad
DefinitionBlock ("", "SSDT", 2, "T420", "PS2K", 0)
{
    // Select specific configuration in VoodooPS2Trackpad.kext
    Method(_SB.PCI0.LPC.PS2K._DSM, 4)
    {
        If (!Arg2) { Return (Buffer() { 0x03 } ) }
        Return (Package()
        {
            "RM,oem-id", "LENOVO",
            "RM,oem-table-id", "T420",
        })
    }
    // Overrides for settings in the Info.plist files)
    // keycodes https://github.com/RehabMan/OS-X-Voodoo-PS2-Controller/blob/master/VoodooPS2Keyboard/ApplePS2ToADBMap.h
    // keycodes detection install https://github.com/RehabMan/OS-X-ioio to /usr/bin and run below command
    // ioio -s ApplePS2Keyboard LogScanCodes 1
    // to see logs launch spotlight >> console filter by ps2
    Name(_SB.PCI0.LPC.PS2K.RMCF, Package()
    {
        "Keyboard", Package()
        {
            "ActionSwipeLeft",  "37 d, 21 d, 21 u, 37 u",
            "ActionSwipeRight", "37 d, 1e d, 1e u, 37 u",
            "SleepPressTime",   "1500",
            "Swap command and option", ">y",
            "Custom ADB Map", Package()
            {
                Package(){},
                "e069=40", //Map Forward to f17
                "e06a=4f", //Map Backward to f18
                "e063=50", //Map Fn to f19
                "3a=5a", //Map cl to f20
            },
            "Custom PS2 Map", Package()
            {
                Package(){},
                "e052=64", //Map ins to f13
                "e037=67", //Map PrntScr to f16
            },
        },
        "Synaptics TouchPad", Package()
        {
            "BogusDeltaThreshX", 800,
            "BogusDeltaThreshY", 800,
            "Clicking", ">y",
            "DragLockTempMask", 0x40004,
            "DynamicEWMode", ">n",
            "FakeMiddleButton", ">n",
            "HWResetOnStart", ">y",
            "ForcePassThrough", ">y",
            "SkipPassThrough", ">y",
            "PalmNoAction When Typing", ">y",
            "ScrollResolution", 800,
            "SmoothInput", ">y",
            "UnsmoothInput", ">y",
            "Thinkpad", ">y",
            "DivisorX", 1,
            "DivisorY", 1,
            "FingerZ", 47,
            "MaxTapTime", 100000000,
            "MomentumScrollThreshY", 16,
            "MouseMultiplierX", 8,
            "MouseMultiplierY", 8,
            "MouseScrollMultiplierX", 2,
            "MouseScrollMultiplierY", 2,
            "MultiFingerHorizontalDivisor", 4,
            "MultiFingerVerticalDivisor", 4,
            "Resolution", 3200,
            "ScrollDeltaThreshX", 10,
            "ScrollDeltaThreshY", 10,
            "TrackpointScrollYMultiplier", 1, //Change this value to 0xFFFF in order to inverse the vertical scroll direction of the Trackpoint when holding the middle mouse button.
            "TrackpointScrollXMultiplier", 1, //Change this value to 0xFFFF in order to inverse the horizontal scroll direction of the Trackpoint when holding the middle mouse button.
        },
    })
}
//EOF
