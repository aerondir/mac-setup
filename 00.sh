#!/bin/bash


# Get the prerequisites
#xcode-select --install
[ "`xcode-select -p 1>/dev/null;echo $?`" == 2 ] && \
    xcodebuild -versionxcode-select --install && \
    sudo xcodebuild -license accept

# Check that Homebrew is installed and install if not
if test ! $(which brew)
then
  echo "  Installing Homebrew for you."
  yes '' | /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi