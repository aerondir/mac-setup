#!/bin/bash

#Setup System Shortcuts
defaults write com.apple.universalaccess com.apple.custommenu.apps -array-add "com.google.Chrome"
defaults write com.google.Chrome NSUserKeyEquivalents '{ "Preferences..." = "^~S";  "Select Next Tab" = "@.";  "Select Previous Tab" = "@,"; }'
defaults write com.jetbrains.intellij NSUserKeyEquivalents '{  "Preferences..." = "^~S"; "Quit IntelliJ IDEA" = "~\UF707";}'
defaults write com.jetbrains.intellij NSUserKeyEquivalents -dict-add "Hide IntelliJ IDEA" "\U200B"
defaults write com.tinyspeck.slackmacgap NSUserKeyEquivalents  '{ "Preferences..." = "^~s";}'
defaults write NSGlobalDomain NSUserKeyEquivalents '{ "Preferences..." = "^~s";}'

#(reboot required & not affecting shortcuts ui json option) Turn Dock Hiding On/Off - Command, Option, D
defaults write com.apple.symbolichotkeys AppleSymbolicHotKeys -dict-add 52 "{ enabled = 0; value = { parameters = ( 100, 2, 1572864 ); type ='standard'; }; }"

#(working xml option) just use 2 reload commands below
#disable cmd+shift+5 Screenshot and recording options
defaults write com.apple.symbolichotkeys.plist AppleSymbolicHotKeys -dict-add 184 "
  <dict>
    <key>enabled</key><false/>
    <key>value</key><dict>
      <key>type</key><string>standard</string>
      <key>parameters</key>
      <array>
        <integer>53</integer>
        <integer>23</integer>
        <integer>1179648</integer>
      </array>
    </dict>
  </dict>
"

#reimport system shortcuts
defaults import com.apple.symbolichotkeys state/symbolichotkeys.plist

#reload system shortcuts
# Ask the system to read the hotkey plist file and ignore the output. Likely updates an in-memory cache with the new plist values.
defaults read com.apple.symbolichotkeys.plist > /dev/null
# Run reactivateSettings to apply the updated settings.
/System/Library/PrivateFrameworks/SystemAdministration.framework/Resources/activateSettings -u

#Copy alt-free English layout
cp x_layout.keylayout ~/Library/Keyboard\ Layouts/

#!!!NB open karabiner first time and allow required security settings

#Copy Karabiner definitions
cp karabiner/*.json ~/.config/karabiner/assets/complex_modifications/
cp karabiner.json ~/.config/karabiner/karabiner.json

#Copy spectacle state
cp state/Shortcuts.json ~/Library/Application\ Support/Spectacle/
