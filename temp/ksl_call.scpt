use framework "Foundation"
use scripting additions
use kLib : script "Keyboard Shortcuts Lib"

-- Add Kamenoko shortcuts.
addGUICustomKeyboardShortcutApplication("jp.piyomarusoft.kamenoko1") of kLib
addCustomKeyboardShortcut("jp.piyomarusoft.kamenoko1", "About Kamenoko", "@^s") of kLib
