#find keycode
brew install  key-codes

defaults export com.apple.symbolichotkeys symbolichotkeys.plist creates a binary plist file (at least as of OSX 10.11); use defaults export com.apple.symbolichotkeys - >symbolichotkeys.plist to create an XML file

defaults import com.apple.symbolichotkeys symbolichotkeys.plist

To write to com.apple.symbolichotkeys defaults you can use from terminal such command:

defaults write com.apple.symbolichotkeys AppleSymbolicHotKeys -dict-add 73 "{enabled =1; value = { parameters = (65535, 53, 1048576); type = 'standard';}; }"

https://stackoverflow.com/questions/823705/programatically-get-set-mac-osx-default-system-keyboard-shortcut

Actually there's a Plist for that, informations are stored in com.apple.symbolichotkeys AppleSymbolicHotKeys which is a complex nested dicts and lists
Let's say you want to programatically modify the "Show Help Menu" shortcut in System Preferences -> Keyboard -> Shortcuts tab -> App Shortcut -> All Applications. To find the correct entry print all the Plist in a text file, modify the shortcut in the System Preferences, print again the the Plist in a second file and diff them:

$ defaults read com.apple.symbolichotkeys AppleSymbolicHotKeys > 1
$ # modify System Preferences
$ defaults read com.apple.symbolichotkeys AppleSymbolicHotKeys > 2
$ diff -U 5 1 2

— Screenshot and recording options
defaults write com.apple.symbolichotkeys AppleSymbolicHotKeys -dict-add 184 "{enabled =0; value = { parameters =  ( 53, 23, 1179648 );  type = standard; }; }"

-- full reload?
defaults read com.apple.symbolichotkeys.plist > /dev/null
/System/Library/PrivateFrameworks/SystemAdministration.framework/Resources/activateSettings -u

other links
https://apple.stackexchange.com/questions/91679/is-there-a-way-to-set-an-application-shortcut-in-the-keyboard-preference-pane-vi
https://apple.stackexchange.com/questions/344494/how-to-disable-default-mission-control-shortcuts-in-terminal/344504
https://gist.github.com/stephancasas/74c4621e2492fb875f0f42778d432973
https://web.archive.org/web/20141112224103/http://hintsforums.macworld.com/showthread.php?t=114785
https://discussions.apple.com/thread/428814
https://discussions.apple.com/thread/251652701

#example of working script
https://github.com/diimdeep/dotfiles/blob/master/osx/configure/hotkeys.sh
>> example_hotkeys.sh

#example of diff macos settings
https://ss64.com/osx/syntax-defaults.html
ss64_defaults.sh

#moar examples
https://github.com/isao/shell/blob/main/osx-defaults.sh

#apple script util to invest.
https://forum.latenightsw.com/t/setting-other-applications-keyboard-shortcuts-using-nsuserdefaults-defaults-not-updating/3537

