How-to: System preference settings for macOS
The preferences below can be applied using defaults

Use at your own risk.

Many of these values can harm your system if used improperly.
Before making changes with defaults write, use defaults read and make a note of any current settings.
defaults delete can be used to remove a preference completely.

Some of these changes require a logout/restart to take effect. All settings are case sensitive.

A boolean value can be given as -bool[ean] TRUE, FALSE, YES or NO (not case sensitive).

Applications do access the defaults system while they're running, so don't modify the defaults of a running application. If you change a default in a domain that belongs to a running application, the application won't see the change and might even overwrite the default. This includes the 'System Preferences' app.

GlobalPreferences
Enable multitouch trackpad auto orientation sensing (for all users)
$ defaults write /Library/Preferences/com.apple.MultitouchSupport ForceAutoOrientation -boolean

Map navigation swipe to 3 fingers (turn this off to get 4-finger navigation swipe)
$ defaults write com.apple.systempreferences com.apple.preference.trackpad.3fdrag-4fNavigate -boolean-neg

Hide Shutdown Button on Login Window:
$ defaults write com.apple.loginwindow ShutDownDisabled -bool true

Remove Restart Button From Login Window:
$ defaults write com.apple.loginwindow RestartDisabled -bool true

Disable creation of Metadata Files on Network Volumes (avoids creation of .DS_Store and AppleDouble ._ files.)
$ defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true

Disable creation of Metadata Files on USB Volumes (avoids creation of .DS_Store and AppleDouble ._ files.)
$ defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true

Disable Login for Hidden User ">Console"
$ defaults write com.apple.loginwindow DisableConsoleAccess -bool true

Add a message to the login screen:
$ sudo defaults write /Library/Preferences/com.apple.loginwindow LoginwindowText "Your Message"

Change login screen background:
$ sudo defaults write /Library/Preferences/com.apple.loginwindow DesktopPicture "/Library/Desktop Pictures/Aqua Blue.jpg"

Disable transparency in the menu bar and elsewhere
Yosemite - equivalent to System Preferences > Accessibility > Display > "Reduce Transparency"
defaults write com.apple.universalaccess reduceTransparency -bool true

Disable Expose
$ defaults write com.apple.dock mcx-expose-disabled -boolean true

In Expose, only show windows from the current space:
$ defaults write com.apple.dock wvous-show-windows-in-other-spaces -bool FALSE
$ killall Dock

In Expose, show other spaces' windows (default):
$ defaults delete com.apple.dock wvous-show-windows-in-other-spaces
$ killall Dock

Enable 64-bit Kernel:
$ defaults write /Library/Preferences/SystemConfiguration/com.apple.Boot.plist Kernel Flags -string

Turn off the “Application Downloaded from Internet” quarantine warning:
$ defaults write com.apple.LaunchServices LSQuarantine -bool NO

Every App
Restore the the 'Save As' menu item (Equivalent to adding a Keyboard shortcut in the System Preferences.app )
$ defaults write -g NSUserKeyEquivalents -dict-add 'Save As...' '@$S'

Save to disk (not to iCloud) by default:
$ defaults write -g NSDocumentSaveNewDocumentsToCloud -bool false

Expand 'Save As…' dialog boxes by default:
defaults write -g NSNavPanelExpandedStateForSaveMode -boolean true
defaults write -g NSNavPanelExpandedStateForSaveMode2 -bool true

Expand print panel dialog boxes by default:
defaults write -g PMPrintingExpandedStateForPrint -boolean true
defaults write -g PMPrintingExpandedStateForPrint2 -bool true

Global User Interface Scale Multiplier:
$ defaults write -g AppleDisplayScaleFactor -float

Holding down a character key will repeat the character:
$ defaults write -g ApplePressAndHoldEnabled -bool false

Holding down a character key will popup a small menu showing accent characters à, á, â etc (default in recent macOS versions):
$ defaults write -g ApplePressAndHoldEnabled -bool true

Enable continuous spell checking everywhere:
$ defaults write -g WebContinuousSpellCheckingEnabled -boolean true

Enable automatic dash replacement everywhere:
$ defaults write -g WebAutomaticDashSubstitutionEnabled -boolean true

Enable automatic text replacement everywhere:
$ defaults write -g WebAutomaticTextReplacementEnabled -boolean true

Quit Printer App after Print Jobs complete:
$ defaults write com.apple.print.PrintingPrefs "Quit When Finished" -bool true

Icon Size for Open Panels:
$ defaults write -g NSNavPanelIconViewIconSizeForOpenMode -number

Keyboard press and hold behavior:
$ defaults write -g ApplePressAndHoldEnabled -boolean true

Monitor font smoothing
Display the current font smoothing:
$ defaults -currentHost read -globalDomain AppleFontSmoothing

Change the font smoothing (0=off, 1=light, 2=Medium/flat panel, 3=strong/blurred)
This is mostly needed for non-Apple displays.
$ defaults -currentHost write -globalDomain AppleFontSmoothing -int 0
Restart applications to see the effect

Delete the setting completely (restore default)
$ defaults -currentHost delete -globalDomain AppleFontSmoothing

Set the font anti-aliasing minimum sizes, globally or per-application:
$ defaults -currentHost write .GlobalPreferences AppleSmoothFontsSizeThreshold -int 16
$ defaults -currentHost write .GlobalPreferences AppleFontSmoothing -int 0
$ defaults -currentHost write com.apple.safari AppleAntiAliasingThreshold -int 16

Address Book
Access the Debug menu in Contacts/Address Book (Set to NO to revert to the default):
$ defaults write com.apple.addressbook ABShowDebugMenu -bool YES

BBEdit
Use old style modal Find Dialog:
$ defaults write com.barebones.bbedit FindDialog:UseOldSk00lFindDialog -boolean true

Control-left-arrow or Control-right-arrow to scroll horizontally (pre-8.5 behavior )
$ defaults write com.barebones.bbedit Editor_ControlArrowCamelCase -bool NO
$ defaults write com.barebones.bbedit Editor_ControlArrowHScroll -bool YES

Control-left-arrow or Control-right-arrow to jump to the next (or previous) transition from lower-case to upper-case characters (or a word boundary, whichever comes first). This is the default:

$ defaults write com.barebones.bbedit Editor_ControlArrowCamelCase -bool YES
$ defaults write com.barebones.bbedit Editor_ControlArrowHScroll -bool NO

Dashboard launcher
Drag widgets out of dashboard (devmode)
$ defaults write com.apple.dashboard devmode -boolean YES/NO

Dictionary
Reuse dictionary definition window:
$ defaults write com.apple.Dictionary ProhibitNewWindowForRequest -boolean YES/NO

DiskUtility
Show hidden Partitions:
$ defaults write com.apple.DiskUtility DUShowEveryPartition -boolean YES/NO

Dock
Show indicator lights for open applications in the dock:
$ defaults write com.apple.dock show-process-indicators -bool true
$ killall Dock

Disable Bouncing dock icons:
$ defaults write com.apple.dock no-bouncing -bool True
$ killall Dock
To re-enable, set this back to False

Minimize windows into their application’s icon
$ defaults write com.apple.dock minimize-to-application -bool true

Make the Dock Icon of any 'Hidden' Applications Dim/Translucent:
$ defaults write com.apple.Dock showhidden -bool YES
$ killall Dock
Disable multi-display swoosh animations
$ defaults write com.apple.dock workspaces-swoosh-animation-off -bool YES && killall Dock
Re-enable with
$ defaults delete com.apple.dock workspaces-swoosh-animation-off && killall Dock

Remove the animation when hiding or showing the dock
$ defaults write com.apple.dock autohide-time-modifier -float 0

Enable iTunes pop-up notifications:
$ defaults write com.apple.dock itunes-notifications -boolean dock

Minimize Dock effect Genie (default)
$ defaults write com.apple.dock mineffect -string "genie" && killall Dock

Minimize Dock effect Scale:
$ defaults write com.apple.dock mineffect -string "scale" && killall Dock

Minimize Dock effect Suck:
$ defaults write com.apple.dock mineffect -string "suck" && killall Dock

Turn the Dock icons translucent for applications that are open but hidden:
$ defaults write com.apple. Dock showhidden -bool YES;
$ killall Dock
(replace 'YES' with 'NO' to disable this feature)

Add a 'Recent Applications' stack to the Dock.
$ defaults write com.apple.dock persistent-others -array-add '{ "tile-data" = { "list-type" = 1; }; "tile-type" = "recents-tile"; }'
$ killall Dock
To remove - just drag out of the dock again

Stacks, Activate Mouse Over Gradient:
$ defaults write com.apple.dock mouse-over-hilte-stack -boolean YES
$ killall Dock
To deactivate set back to NO

Delete the icon cache to force a rebuild:
$ rm /var/folders/*/*/*/com.apple.dock.iconcache; killall Dock

Finder
Show hidden files:
$ defaults write com.apple.finder AppleShowAllFiles TRUE
$ killall Finder

Hide hidden files (default)
$ defaults write com.apple.finder AppleShowAllFiles FALSE
$ killall Finder

Show all filename extensions
$ defaults write NSGlobalDomain AppleShowAllExtensions -bool true

Disable the macOS Crash reporter (quit dialog after an application crash)
$ defaults write com.apple.CrashReporter DialogType none
To enable the crash reporter (default) change none to prompt

Disable dashboard widgets (saves RAM)
$ defaults write com.apple.dashboard mcx-disabled -boolean YES
To Enable dashboard widgets (default) set this to NO

Disable the New Window Animation:
$ defaults write NSGlobalDomain NSAutomaticWindowAnimationsEnabled -bool NO
To enable the animation (default) set this to YES

# Show the Status Bar, this can also be toggled on the View menu:
$ defaults write com.apple.finder ShowStatusBar -bool true
# Hide the Status Bar (Default)
$ defaults write com.apple.finder ShowStatusBar -bool false

Start the status bar Path at $HOME (instead of Hard drive)
$ defaults write /Library/Preferences/com.apple.finder PathBarRootAtHome -bool TRUE/FALSE

Enable Remote Disc (1)
$ defaults write com.apple.NetworkBrowser EnableODiskBrowsing -boolean true
Enable Remote Disc (2)
$ defaults write com.apple.NetworkBrowser ODSSupported -boolean true

Enable movie preview thumbnails:
$ defaults write com.apple.QuickLookDaemon QLDisableQTThumbnails -boolean NO

Playable icon previews (Movie/PDF files) Minimum size in pixels, default is 64:
Setting this to a large value will effectively disable the preview.
$ defaults write com.apple.finder QLInlinePreviewMinimumSupportedSize -integer

Auto stop previews when scrolling:
$ defaults write com.apple.finder AutoStopWhenScrollingOffBounds -boolean TRUE

Auto stop previews when selection changes:
$ defaults write com.apple.finder AutoStopWhenSelectionChanges -boolean TRUE

Show X-ray for folders with custom icons:
$ defaults write com.apple.DisableXRayForFoldersWithCustomIcons -boolean NO

Show X-ray folders in QuickLook:
$ defaults write com.apple.finder QLEnableXRayFolders -boolean TRUE; killall Finder

Enable slow motion with shift key on QuickLook:
$ defaults write com.apple.finder QLEnableSlowMotion -boolean TRUE

$ Hide QuickLook on deactivation or application switch:
$ defaults write com.apple.finder QLHidePanelOnDeactivate -boolean TRUE

When performing a search, search the current folder by default (the default 'This Mac' is "SCev")
$ defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

Show version column in search results:
$ defaults write com.apple.finder SearchViewSettings.ListViewSettings.columns.version.visible -boolean TRUE

Show label column in search results:
$ defaults write com.apple.finder SearchViewSettings.ListViewSettings.columns.label.visible -boolean TRUE

Show comments column in search results:
$ defaults write com.apple.finder SearchViewSettings.ListViewSettings.columns.comments.visible -boolean TRUE

Show size column in search results:
$ defaults write com.apple.finder SearchViewSettings.ListViewSettings.columns.size.visible -boolean TRUE

Set the sidebar icon size to small, medium or Large:
$ defaults write NSGlobalDomain NSTableViewDefaultSizeMode -int 1
$ defaults write NSGlobalDomain NSTableViewDefaultSizeMode -int 2
$ defaults write NSGlobalDomain NSTableViewDefaultSizeMode -int 1

Disable the warning when changing a file extension
$ defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

Use white text for desktop icons:
$ defaults write com.apple.finder Desktop.HasDarkBackground -boolean TRUE

Show stripes in list view:
$ defaults write com.apple.finder FXListViewStripes -boolean TRUE; killall Finder

Allow quitting Finder like any other app, quitting Finder will also hide all desktop icons.
This is only really useful when using a 3rd party file manager:
$ defaults write com.apple.finder QuitMenuItem -bool YES; killall Finder
Front Row launcher
Set the preferred display to use:
$ defaults write com.apple.frontrow FrontRowUsePreferredDisplayID -integer

iCal
Log HTTP Activity:
$ defaults write com.apple.iCal LogHTTPActivity -boolean TRUE

iTunes 10
Make the arrows next to artist & album jump to local iTunes library folders instead of Store:
$ defaults write com.apple.iTunes show-store-link-arrows -bool YES
$ defaults write com.apple.iTunes invertStoreLinks -bool YES
To restore the default change the YES to NO

Restore the standard close/minimise buttons:
$ defaults write com.apple.iTunes full-window -1

Hide the iTunes Genre list:
$ defaults write com.apple.iTunes show-genre-when-browsing -bool FALSE

Mail
Close Apple Mail before making any changes.

Set a minimum font size of 14px (affects reading and sending email):
$ defaults write com.apple.mail MinimumHTMLFontSize 14

Force all Mail messages to display as plain text:
$ defaults write com.apple.mail PreferPlainText -bool TRUE
For rich text (the default) set it to FALSE

Disable tracking of Previous Recipients:
$ defaults write com.apple.mail SuppressAddressHistory -boolean TRUE

Send Windows friendly attachments:
$ defaults write com.apple.mail SendWindowsFriendlyAttachments -boolean TRUE

Mouse
Enable momentum scrolling for Magic Mouse:
$ defaults write com.apple.driver.AppleBluetoothMultitouch.mouse MouseMomentumScroll -boolean TRUE

OmniGraffle
Allow scroll wheel zooming:
$ defaults write com.omnigroup.OmniGraffle DisableScrollWheelZooming -boolean NO

Allow scroll wheel zooming in OmniGrafflePro:
$ defaults write com.omnigroup.OmniGrafflePro DisableScrollWheelZooming -boolean NO

Preview
Scale images by default when printing:
$ defaults write write com.apple.Preview PVImagePrintingScaleMode -boolean TRUE

Preview Auto-rotate by default when printing:
$ defaults write write com.apple.Preview PVImagePrintingAutoRotate -boolean TRUE

Quit Always Keeps Windows:
$ defaults write com.apple.Preview NSQuitAlwaysKeepsWindows -boolean TRUE

Quick Time Player
Automatically show Closed Captions (CC) when opening a Movie:
$ defaults -currentHost write com.apple.QuickTimePlayerX.plist MGEnableCCAndSubtitlesOnOpen -bool TRUE

Safari
Enable encoding detector:
$ defaults write com.apple.Safari WebKitUsesEncodingDetector -boolean TRUE

Tooltip Font Size:
$ defaults write com.apple.Safari NSToolTipsFontSize -float

Enable debug menu:
$ defaults write com.apple.Safari IncludeDebugMenu -boolean TRUE

ScreenSharing
Debug (To Show Bonjour)
$ defaults write com.apple.ScreenSharing debug -boolean

Do Not Send Special Keys to Remote Machine
$ defaults write com.apple.ScreenSharing DoNotSendSystemKeys -boolean TRUE

Skip local address check:
$ defaults write com.apple.ScreenSharing skipLocalAddressCheck -boolean TRUE

Screen sharing image quality for Apple's built-in VNC server:
1 = black and white  2 = grayscale  3 = 8-bit color  4 = 16-bit color  5 = full color
$ defaults write com.apple.ScreenSharing controlObserveQuality -integer 5

Screen Capture
Remove the drop shadows from saved screenshots (Mojave and higher):
defaults write com.apple.screencapture disable-shadow -bool TRUE && killall SystemUIServer

Restore the default drop shadows:
defaults write com.apple.screencapture disable-shadow -bool FALSE && killall SystemUIServer

Set the screenshot location to the Desktop (default):
defaults write com.apple.screencapture "location" -string "~/Desktop" && killall SystemUIServer

Set the screenshot location to ~/Pictures
defaults write com.apple.screencapture "location" -string "~/Pictures" && killall SystemUIServer

Read the current screenshot location:
defaults read com.apple.screencapture "location"

Restore the default screenshot location:
defaults delete com.apple.screencapture "location" && killall SystemUIServer
Software update
While applying any changes to SoftwareUpdate defaults, set software update to OFF to avoid any conflict with the defaults system cache. (Also close the System Preferences app)

sudo softwareupdate --schedule OFF
<apply changes>
sudo softwareupdate --schedule ON

Automatically check for updates (required for any downloads):
$ sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticCheckEnabled -bool YES

Download updates automatically in the background
$ sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticDownload -bool YES

Install app updates automatically:
$ sudo defaults write /Library/Preferences/com.apple.commerce AutoUpdate -bool YES

Don't Install macos updates automatically
$ sudo defaults write /Library/Preferences/com.apple.commerce AutoUpdateRestartRequired -bool false

Install system data file updates automatically:
$ sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist ConfigDataInstall -bool YES

Install critical security updates automatically:
$ sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist CriticalUpdateInstall -bool YES

Spaces
When switching applications, switch to respective space
$ defaults write -g AppleSpacesSwitchOnActivate -bool true

RemoteManagement
Disable Multicast:
$ defaults write /Library/Preferences/com.apple.RemoteManagement ARD_MulticastAllowed -boolean

Remote Desktop
Set the maximum number of computers that can be observed:
$ defaults write com.apple.RemoteDesktop multiObserveMaxPerScreen -integer 9
(up to 50 opposed to the default of 9)
Terminal
Terminal windows take focus with mouse over window:
$ defaults write com.apple.terminal FocusFollowsMouse -string YES

Terminal windows take focus with click (default)
$ defaults write com.apple.terminal FocusFollowsMouse -string NO

TimeMachine
Auto backup:
$ defaults write com.apple.TimeMachine AutoBackup =1

Backup frequency default= 3600 seconds (every hour) 1800 = 1/2 hour, 7200=2 hours
$ sudo defaults write /System/Library/Launch Daemons/com.apple.backupd-auto StartInterval -int 1800

Related macOS commands:

defaults - set preferences, for both macOS and individual applications.
