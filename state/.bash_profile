# enable color support of ls and also add handy aliases
# ~/.dircolors/themefile
HISTFILESIZE=2000
eval $(gdircolors ~/.dircolors/dircolors.ansi-light)
alias ls='gls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
#export CLICOLOR=YES
function findstack() {
    if [ -z "$1" ]; then
        echo "Usage: $0 [physical resource id]";
        return 1;
    fi

    aws cloudformation describe-stack-resources --physical-resource-id $1 --query "StackResources[0].StackName" --output text
}
#npm install -g aws-azure-login
awslogin () {
	if [ "$1" != "" ]
	then
		aws-azure-login --no-prompt --profile "$1"
		export AWS_PROFILE="$1"
	else
		aws-azure-login --no-prompt --profile default 
		export AWS_PROFILE=default
	fi
}
dlogin () {
	if [ "$1" != "" ]
	then
		docker run --rm -it -v ~/.aws:/root/.aws sportradar/aws-azure-login --no-prompt --profile "$1"
		export AWS_PROFILE="$1"
	else
		docker run --rm -it -v ~/.aws:/root/.aws sportradar/aws-azure-login --no-prompt --profile default 
		export AWS_PROFILE=default
	fi
}
ssm() {
	aws ssm start-session --target "$1"
}
snowsql() {
	/Applications/SnowSQL.app/Contents/MacOS/snowsql --client-session-keep-alive --authenticator="oauth" --token="$1"
}
alias ecs='ecs-cli'
export AWS_SHARED_CREDENTIALS_FILE="$HOME/.aws/credentials"
export PATH="/usr/local/opt/python@3.7/bin:$PATH"
export PATH="~/Library/Python/3.7/bin/:$PATH"
export PATH="~/bin/sessionmanager-bundle/bin:$PATH"
export CODEBASE=.
export PS1='\[\033[G\][\[\e[32m\]\t\[\e[0m\]] \[\e[36m\]\w \[\e[33m\]$(git branch 2>/dev/null | sed -n "s/* \(.*\)/\1 /p")\[\e[0m\]$ '

PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$ '
export PS1='\[\033[G\][\[\e[32m\]\t\[\e[0m\]] \[\e[36m\]\w \[\e[33m\]master \[\e[0m\]$ '