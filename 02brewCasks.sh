#!/bin/bash

echo Installing brew cask binaries
#brew install google-chrome
brew install firefox
brew install iterm2
brew install intellij-idea
brew install karabiner-elements
#brew install slack
#brew install skype
brew install spectacle
brew install macfuse
brew install Virtuslab/scala-cli/scala-cli
curl -s "https://get.sdkman.io" | bash
