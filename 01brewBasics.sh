#!/bin/bash

# curl -o ~/.osx https://*/.osx && bash ~/.osx

# Set a shorter delay until key repeat:
defaults write NSGlobalDomain InitialKeyRepeat -int 15

# Set a blazingly fast keyboard repeat rate:
defaults write NSGlobalDomain KeyRepeat -int 2

##########
# Config #
##########

# Get the command line tools!
[ "`xcode-select -p 1>/dev/null;echo $?`" == 2 ] && \
    xcodebuild -versionxcode-select --install && \
    sudo xcodebuild -license accept

# Check that Homebrew is installed and install if not
if test ! $(which brew)
then
  echo "  Installing Homebrew for you."
  yes '' | /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

# Update any existing homebrew recipes
brew update

# Upgrade any already installed formulae
brew upgrade

# Install my brew packages
brew install bash
brew install wget
brew install git bash-completion
brew install vim
brew install midnight-commander
brew install openssl
brew install sshfs jq

echo -e "\r\n/usr/local/bin/bash" | sudo tee -a /etc/shells
chsh -s /usr/local/bin/bash

