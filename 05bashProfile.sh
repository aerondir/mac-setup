#just use https://github.com/gibbling/dircolors
brew install coreutils
mkdir ~/.dircolors
git clone https://github.com/gibbling666/dircolors.git ~/.dircolors

cat <<EOT >> ~/.bash_profile
PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
# enable color support of ls and also add handy aliases
# ~/.dircolors/themefile
eval \$(gdircolors ~/.dircolors/dircolors.ansi-light)
alias ls='gls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
EOT

echo "export PS1='\[\033[G\][\[\e[32m\]\t\[\e[0m\]] \[\e[36m\]\w \[\e[33m\]$(git branch 2>/dev/null | sed -n "s/* \(.*\)/\1 /p")\[\e[0m\]$ '" >> ~/.bash_profile
